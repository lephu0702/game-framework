﻿using UnityEngine;

namespace Framework.PlayerPrefs {
    public interface IPlayerPrefs {
        bool SupportsSecurePrefs { get; }
        bool UseSecurePrefs { get; set; }
        void DeleteAll();
        void DeleteKey(string key, bool? useSecurePrefs = null);
        float GetFloat(string key, float defaultValue = 0.0f, bool? useSecurePrefs = null);
        int GetInt(string key, int defaultValue = 0, bool? useSecurePrefs = null);
        string GetString(string key, string defaultValue = "", bool? useSecurePrefs = null);
        bool GetBool(string key, bool defaultValue = false, bool? useSecurePrefs = null);
        Vector2? GetVector2(string key, Vector2? defaultValue = null, bool? useSecurePrefs = null);
        Vector3? GetVector3(string key, Vector3? defaultValue = null, bool? useSecurePrefs = null);
        Color? GetColor(string key, Color? defaultValue = null, bool? useSecurePrefs = null);
        bool HasKey(string key, bool? useSecurePrefs = null);
        void Save();
        void SetFloat(string key, float value, bool? useSecurePrefs = null);
        void SetInt(string key, int value, bool? useSecurePrefs = null);
        void SetString(string key, string value, bool? useSecurePrefs = null);
        void SetBool(string key, bool value, bool? useSecurePrefs = null);
        void SetVector2(string key, Vector2 value, bool? useSecurePrefs = null);
        void SetVector3(string key, Vector3 value, bool? useSecurePrefs = null);
        void SetColor(string key, Color value, bool? useSecurePrefs = null);
    }
}