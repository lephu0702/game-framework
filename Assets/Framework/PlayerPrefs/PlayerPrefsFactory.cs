﻿using UnityEngine;

namespace Framework.PlayerPrefs {
    public static class PlayerPrefsFactory {
        static IPlayerPrefs _instance;

        static IPlayerPrefs Instance {
            get {
                if (_instance == null)
                    _instance = new PlayerPrefsHandler();
                return _instance;
            }
        }

        public static bool SupportsSecurePrefs => Instance.SupportsSecurePrefs;

        public static bool UseSecurePrefs {
            get => Instance.UseSecurePrefs;
            set => Instance.UseSecurePrefs = value;
        }

        public static void DeleteAll() {
            Instance.DeleteAll();
        }

        public static void DeleteKey(string key) {
            Instance.DeleteKey(key);
        }

        public static float GetFloat(string key, float defaultValue = 0.0f, bool? useSecurePrefs = null) {
            return Instance.GetFloat(key, defaultValue, useSecurePrefs);
        }

        public static int GetInt(string key, int defaultValue = 0, bool? useSecurePrefs = null) {
            return Instance.GetInt(key, defaultValue, useSecurePrefs);
        }

        public static string GetString(string key, string defaultValue = "", bool? useSecurePrefs = null) {
            return Instance.GetString(key, defaultValue, useSecurePrefs);
        }

        public static bool GetBool(string key, bool defaultValue = false, bool? useSecurePrefs = null) {
            return Instance.GetBool(key, defaultValue, useSecurePrefs);
        }

        public static Vector2? GetVector2(string key, Vector2? defaultValue = null, bool? useSecurePrefs = null) {
            return Instance.GetVector2(key, defaultValue, useSecurePrefs);
        }

        public static Vector3? GetVector3(string key, Vector3? defaultValue = null, bool? useSecurePrefs = null) {
            return Instance.GetVector3(key, defaultValue, useSecurePrefs);
        }

        public static Color? GetColor(string key, Color? defaultValue = null, bool? useSecurePrefs = null) {
            return Instance.GetColor(key, defaultValue, useSecurePrefs);
        }

        public static bool HasKey(string key) {
            return Instance.HasKey(key);
        }

        public static void Save() {
            Instance.Save();
        }

        public static void SetFloat(string key, float value, bool? useSecurePrefs = null) {
            Instance.SetFloat(key, value, useSecurePrefs);
        }

        public static void SetInt(string key, int value, bool? useSecurePrefs = null) {
            Instance.SetInt(key, value, useSecurePrefs);
        }

        public static void SetString(string key, string value, bool? useSecurePrefs = null) {
            Instance.SetString(key, value, useSecurePrefs);
        }

        public static void SetBool(string key, bool value, bool? useSecurePrefs = null) {
            Instance.SetBool(key, value, useSecurePrefs);
        }

        public static void SetVector2(string key, Vector2 value, bool? useSecurePrefs = null) {
            Instance.SetVector2(key, value, useSecurePrefs);
        }

        public static void SetVector3(string key, Vector3 value, bool? useSecurePrefs = null) {
            Instance.SetVector3(key, value, useSecurePrefs);
        }

        public static void SetColor(string key, Color value, bool? useSecurePrefs = null) {
            Instance.SetColor(key, value, useSecurePrefs);
        }
    }
}