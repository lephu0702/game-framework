﻿using UnityEngine;

namespace Framework.PlayerPrefs {
    public class PlayerPrefsHandler : IPlayerPrefs {
        public bool SupportsSecurePrefs => false;

        public bool UseSecurePrefs { get; set; }

        public void DeleteAll() {
            UnityEngine.PlayerPrefs.DeleteAll();
        }

        public void DeleteKey(string key, bool? useSecurePrefs = null) {
            UnityEngine.PlayerPrefs.DeleteKey(key);
        }

        public float GetFloat(string key, float defaultValue = 0, bool? useSecurePrefs = null) {
            return UnityEngine.PlayerPrefs.GetFloat(key, defaultValue);
        }

        public int GetInt(string key, int defaultValue = 0, bool? useSecurePrefs = null) {
            return UnityEngine.PlayerPrefs.GetInt(key, defaultValue);
        }

        public string GetString(string key, string defaultValue = "", bool? useSecurePrefs = null) {
            return UnityEngine.PlayerPrefs.GetString(key, defaultValue);
        }

        public bool GetBool(string key, bool defaultValue = false, bool? useSecurePrefs = null) {
            return defaultValue;
        }

        public Vector2? GetVector2(string key, Vector2? defaultValue = null, bool? useSecurePrefs = null) {
            return defaultValue;
        }

        public Vector3? GetVector3(string key, Vector3? defaultValue = null, bool? useSecurePrefs = null) {
            return defaultValue;
        }

        public Color? GetColor(string key, Color? defaultValue = null, bool? useSecurePrefs = null) {
            return defaultValue;
        }

        public bool HasKey(string key, bool? useSecurePrefs = null) {
            return UnityEngine.PlayerPrefs.HasKey(key);
        }

        public void Save() {
            UnityEngine.PlayerPrefs.Save();
        }

        public void SetFloat(string key, float value, bool? useSecurePrefs = null) {
            UnityEngine.PlayerPrefs.SetFloat(key, value);
        }

        public void SetInt(string key, int value, bool? useSecurePrefs = null) {
            UnityEngine.PlayerPrefs.SetInt(key, value);
        }

        public void SetString(string key, string value, bool? useSecurePrefs = null) {
            UnityEngine.PlayerPrefs.SetString(key, value);
        }

        public void SetBool(string key, bool value, bool? useSecurePrefs = null) {
        }

        public void SetVector2(string key, Vector2 value, bool? useSecurePrefs = null) {
        }

        public void SetVector3(string key, Vector3 value, bool? useSecurePrefs = null) {
        }

        public void SetColor(string key, Color value, bool? useSecurePrefs = null) {
        }
    }
}