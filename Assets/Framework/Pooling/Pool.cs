﻿using System.Collections.Generic;
using UnityEngine;

namespace Framework.Pooling {
    public class Pool {
        #region Enums

        #endregion Enums

        #region Inspector Variables

        [SerializeField] GameObject _prefab;

        public GameObject Prefab {
            get => _prefab;
            set => _prefab = value;
        }

        [SerializeField] int _initialSize = 1;

        public int InitialSize {
            get => _initialSize;
            set => _initialSize = value;
        }

        [SerializeField] int _maximumSize;

        public int MaximumSize {
            get => _maximumSize;
            set => _maximumSize = value;
        }

        Queue<PoolItem> _inactiveInstances;
        [System.NonSerialized] List<PoolItem> _activeInstances;
        Dictionary<GameObject, PoolItem> _spawnedGameObjectToPoolItemLookup;

        #endregion Inspector Variables

        public Pool() {
            _inactiveInstances = new Queue<PoolItem>(MaximumSize);
            _activeInstances = new List<PoolItem>(MaximumSize);
            _spawnedGameObjectToPoolItemLookup = new Dictionary<GameObject, PoolItem>(MaximumSize);
        }

        #region Init and Cleanup

        #endregion Init and Cleanup

        #region Despawn

        #endregion Despawn

        #region Create

        PoolItem CreatePoolInstance(Transform parent, Vector3 position, Quaternion rotation) {
            var prefabClone = Object.Instantiate(Prefab, position, rotation);
            if (parent != null)
                prefabClone.transform.SetParent(parent, true);
            prefabClone.name = Prefab.name;

            var poolInstance = CreatePoolItem();
            poolInstance.GameObject = prefabClone;
            poolInstance.OriginalPrefab = Prefab;
            poolInstance.Pool = this;
            poolInstance.OnSetup();
            return poolInstance;
        }

        protected virtual PoolItem CreatePoolItem() {
            return new PoolItem();
        }

        #endregion Create
    }
}