﻿using UnityEngine;

namespace Framework.Pooling {
    [System.Serializable]
    public class PoolItem {
        public Pool Pool { get; set; }
        public GameObject OriginalPrefab { get; set; }
        public GameObject GameObject { get; set; }
        public bool IsFromPool { get; set; }

        IPoolComponent[] _poolComponents;

        public virtual void OnSetup() {
            _poolComponents = GameObject.GetComponents<IPoolComponent>();
        }

        public virtual void OnSpawn() {
            for (var i = 0; i < _poolComponents.Length; i++) {
                _poolComponents[i].OnSpawned(this);
            }
        }

        public virtual void OnDespawn() {
            for (var i = 0; i < _poolComponents.Length; i++) {
                _poolComponents[i].OnDespawned(this);
            }
        }

        public virtual void OnDestroy() {
        }

        public virtual void DespawnSelf() {
            // Pool.Despawn(this);
        }
    }
}