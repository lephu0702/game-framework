﻿namespace Framework.Pooling {
    public interface IPoolComponent {
        void OnSpawned(PoolItem poolItem);
        void OnDespawned(PoolItem poolItem);
    }
}