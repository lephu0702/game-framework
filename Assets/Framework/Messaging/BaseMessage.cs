﻿using System.Diagnostics;

namespace Framework.Messaging {
    public class BaseMessage {
        public enum SendModeType {
            SendToAll,
            SendToFirst
        }

        public string Name;
        public SendModeType SendMode;

#if UNITY_EDITOR
        public bool DontShowInEditorLogInternal;
#endif

        public BaseMessage() {
            Name = GetType().Name;
            SendMode = SendModeType.SendToAll;
        }

        [Conditional("UNITY_EDITOR")]
        public void DontShowInEditorLog() {
#if UNITY_EDITOR
            DontShowInEditorLogInternal = true;
#endif
        }
    }
}