﻿using System;
using System.Collections.Generic;

namespace Framework.Messaging {
    public class Messenger {
        public delegate bool MessageListenerDelegate(BaseMessage message);

        readonly Dictionary<string, List<MessageListenerDelegate>> _listeners =
            new Dictionary<string, List<MessageListenerDelegate>>();

        public void AddListener(Type messageType, MessageListenerDelegate handler) {
            var messageName = messageType.Name;
            if (!_listeners.ContainsKey(messageName)) {
                _listeners.Add(messageName, new List<MessageListenerDelegate>());
            }

            List<MessageListenerDelegate> listenerList = _listeners[messageName];
            listenerList.Add(handler);
        }

        public void RemoveListener(Type messageType, MessageListenerDelegate handler) {
            var messageName = messageType.Name;
            List<MessageListenerDelegate> listenerList = _listeners[messageName];
            listenerList.Remove(handler);
        }

        public void AddListener<T>(MessageListenerDelegate handler) where T : BaseMessage {
            var messageType = typeof(T);
            AddListener(messageType, handler);
        }

        public void RemoveListener<T>(MessageListenerDelegate handler) where T : BaseMessage {
            var messageType = typeof(T);
            RemoveListener(messageType, handler);
        }

        public bool TriggerMessage(BaseMessage msg) {
            if (!_listeners.ContainsKey(msg.Name)) {
                return false;
            }

            List<MessageListenerDelegate> listenerList = _listeners[msg.Name];
            for (int i = 0; i < listenerList.Count; ++i) {
                var sent = listenerList[i](msg);

                if (msg.SendMode == BaseMessage.SendModeType.SendToFirst && sent) {
                    return true;
                }
            }

            return true;
        }
    }
}