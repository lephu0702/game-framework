﻿using System;
using System.Collections;
using System.Collections.Generic;
using Framework.GameObjects;
using Framework.Messaging;
using UnityEngine;

namespace Framework.Game {
    public class GameManager : SingletonPersistant<GameManager> {
        readonly Messenger _messenger = new Messenger();

        public static Messenger Messenger => IsActive ? Instance._messenger : null;

        #region Messaging

        public static bool AddListener<T>(Messenger.MessageListenerDelegate handler) where T : BaseMessage {
            if (!IsActive || Messenger == null) return false;
            Messenger.AddListener<T>(handler);
            return true;
        }

        public static bool RemoveListener<T>(Messenger.MessageListenerDelegate handler) where T : BaseMessage {
            if (!IsActive || Messenger == null) return false;
            Messenger.RemoveListener<T>(handler);
            return true;
        }

        public static bool AddListener(Type messageType, Messenger.MessageListenerDelegate handler) {
            if (!IsActive || Messenger == null) return false;
            Messenger.AddListener(messageType, handler);
            return true;
        }

        public static bool RemoveListener(Type messageType, Messenger.MessageListenerDelegate handler) {
            if (!IsActive || Messenger == null) return false;
            Messenger.RemoveListener(messageType, handler);
            return true;
        }

        public static bool TriggerMessage(BaseMessage msg) {
            if (!IsActive || Messenger == null) return false;
            return Messenger.TriggerMessage(msg);
        }

        #endregion
    }
}