﻿using UnityEngine;

namespace Framework.GameObjects {
    public class Singleton<T> : MonoBehaviour where T : Component {
        public static T Instance { get; private set; }
        public string TypeName { get; private set; }

        public static bool IsActive => Instance != null;

        void Awake() {
            TypeName = typeof(T).FullName;

            if (Instance != null) {
                if (Instance != this)
                    Destroy(gameObject);
                return;
            }

            Instance = this as T;

            GameSetup();
        }

        void OnDestroy() {
            if (Instance == this) {
                SaveState();
                GameDestroy();
            }
        }

        protected virtual void OnApplicationQuit() {
            SaveState();
        }

        protected virtual void OnApplicationPause(bool pauseStatus) {
            SaveState();
        }

        protected virtual void GameSetup() {
        }

        protected virtual void SaveState() {
        }

        protected virtual void GameDestroy() {
        }
    }
}