﻿using UnityEngine;

namespace Framework.GameObjects {
    public class SingletonPersistant<T> : Singleton<T> where T : Component {
        protected override void GameSetup() {
            DontDestroyOnLoad(gameObject);
            base.GameSetup();
        }
    }
}